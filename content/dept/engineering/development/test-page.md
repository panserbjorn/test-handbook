# Dogfooding

This should be a page that can be tested for content. We might have content that is **bold**, content that is *italic* or content that is _emphasized_. 

We can also use Tables:

|column1|column2|
|:-----:|:-----:|
|value1|value2|

## Dogfooding Antipatterns

An easy antipattern to fall into is to resolve your problem outside of what the product offers. Dogfooding is not:

1. Building a bot outside of GitLab.
2. Writing scripts that leverage the GitLab API (if the functionality is on our roadmap and could be shipped within the GitLab Project).
3. Using a component of GitLab that is part of our components or managed apps.
4. Using templates or repos that are not part of the default UI (having to type or copy-paste to add them).

## Dogfooding Process

Follow the dogfooding process described in the Product Handbook when considering building a tool outside of GitLab.
